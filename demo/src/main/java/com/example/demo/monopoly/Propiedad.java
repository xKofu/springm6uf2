package com.example.demo.monopoly;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "propiedad")
public class Propiedad {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_propiedad")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="precio")
	private int precio;
	
	@Column(name="alquiler")
	private int alquiler;
	
	@Column(name="alquiler1")
	private int alquiler1;
	
	@Column(name="alquiler2")
	private int alquiler2;
	
	@Column(name="alquiler3")
	private int alquiler3;
	
	@Column(name="alquiler4")
	private int alquiler4;
	
	@Column(name="alquiler_hotel")
	private int alquilerHotel;
	
	@Column(name="precio_casa")
	private int precioCasa;
	
	@Column(name="numero_casas")
	private int numCasas;
	
	@Column(name="precio_hipoteca")
	private int precioHipoteca;
	
	@Column(name="hipotecado")
	private boolean hipotecado;
	
	@Column(name="posicion")
	private int posicion;

	//ManyToOne. Aqu declarem la clau foranea.
	//La clau foranea es una referencia a un objecte de classe del que es One
	//a la columa no anir l'objecte sencer, si no que anira la clau primaria
	//nullable false significa que mai pot ser null.
	//JoinColumn(name = NOMBRE DE LA VARIABLE DE -SQL- QUE QUIERES DARLE)
	@ManyToOne
	@JoinColumn(name="id_propietario", nullable=true)
	private Propietario propietario;
	
	@ManyToOne
	@JoinColumn(name="id_color", nullable=false) 
	private Color color;

	public Propiedad() {
		super();
	}
		
	public Propiedad(String nombre) {
		super();
		this.nombre = nombre;
		this.hipotecado = false;
		this.numCasas = 0;
	}

	public Propietario getPropietario() {
		return propietario;
	}

	public void setPropietario(Propietario propietario) {
		this.propietario = propietario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getAlquiler() {
		return alquiler;
	}

	public void setAlquiler(int alquiler) {
		this.alquiler = alquiler;
	}

	public int getAlquiler1() {
		return alquiler1;
	}

	public void setAlquiler1(int alquiler1) {
		this.alquiler1 = alquiler1;
	}

	public int getAlquiler2() {
		return alquiler2;
	}

	public void setAlquiler2(int alquiler2) {
		this.alquiler2 = alquiler2;
	}

	public int getAlquiler3() {
		return alquiler3;
	}

	public void setAlquiler3(int alquiler3) {
		this.alquiler3 = alquiler3;
	}

	public int getAlquiler4() {
		return alquiler4;
	}

	public void setAlquiler4(int alquiler4) {
		this.alquiler4 = alquiler4;
	}

	public int getAlquilerHotel() {
		return alquilerHotel;
	}

	public void setAlquilerHotel(int alquilerHotel) {
		this.alquilerHotel = alquilerHotel;
	}

	public int getPrecioCasa() {
		return precioCasa;
	}

	public void setPrecioCasa(int precioCasa) {
		this.precioCasa = precioCasa;
	}

	public int getNumCasas() {
		return numCasas;
	}

	public void setNumCasas(int numCasas) {
		this.numCasas = numCasas;
	}

	public int getPrecioHipoteca() {
		return precioHipoteca;
	}

	public void setPrecioHipoteca(int precioHipoteca) {
		this.precioHipoteca = precioHipoteca;
	}

	public boolean isHipotecado() {
		return hipotecado;
	}

	public void setHipotecado(boolean hipotecado) {
		this.hipotecado = hipotecado;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public int getId() {
		return id;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Propiedades [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", alquiler=" + alquiler
				+ ", alquiler1=" + alquiler1 + ", alquiler2=" + alquiler2 + ", alquiler3=" + alquiler3 + ", alquiler4="
				+ alquiler4 + ", alquilerHotel=" + alquilerHotel + ", precioCasa=" + precioCasa + ", numCasas="
				+ numCasas + ", precioHipoteca=" + precioHipoteca + ", hipotecado=" + hipotecado + ", posicion="
				+ posicion + "]";
	}
	
}
