package com.example.demo.monopoly;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ferrocarril")
public class Ferrocarril extends Propiedad {

	@Column(name="precio")
	private int precio;
	
	@Column(name="hipoteca")
	private int hipoteca;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(
	name = "Ferrocarril_vecino",
	joinColumns = { @JoinColumn(name = "id_ferrocarril") },
	inverseJoinColumns = { @JoinColumn(name = "id_vecino") }
	)
	private Set<Ferrocarril> MisVecinos;
	
	@ManyToMany(mappedBy = "MisVecinos")
	private Set<Ferrocarril> SoyVecinoDe;
		
	public Ferrocarril() {
		super();
		MisVecinos = new HashSet<Ferrocarril>();
		SoyVecinoDe = new HashSet<Ferrocarril>();
	}
	
	
	public Ferrocarril(String nom) {
		super(nom);
		this.MisVecinos = new HashSet<Ferrocarril>();
		this.SoyVecinoDe = new HashSet<Ferrocarril>();
	}


	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getHipoteca() {
		return hipoteca;
	}

	public void setHipoteca(int hipoteca) {
		this.hipoteca = hipoteca;
	}

	public Set<Ferrocarril> getMisVecinos() {
		return MisVecinos;
	}

	public void setMisVecinos(Set<Ferrocarril> misVecinos) {
		MisVecinos = misVecinos;
	}

	public Set<Ferrocarril> getSoyVecinoDe() {
		return SoyVecinoDe;
	}

	public void setSoyVecinoDe(Set<Ferrocarril> soyVecinoDe) {
		SoyVecinoDe = soyVecinoDe;
	}

		
	
	/*
	public Ferrocarril getVecino1() {
		return vecino1;
	}

	public void setVecino1(Ferrocarril vecino1) {
		this.vecino1 = vecino1;
	}

	public Ferrocarril getVecino2() {
		return vecino2;
	}

	public void setVecino2(Ferrocarril vecino2) {
		this.vecino2 = vecino2;
	}
	*/
	
}
