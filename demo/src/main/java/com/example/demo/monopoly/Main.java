package com.example.demo.monopoly;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import com.example.demo.monopoly.dao.ColorDao;
import com.example.demo.monopoly.dao.PropiedadDao;
import com.example.demo.monopoly.dao.PropietarioDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Main {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	static Random r = new Random();
	static int turno = 0;
	static Scanner sc;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {

		boolean flag = true;

		while (flag) {

			Propietario JugActual = TurnBeggining();

			System.out.println("Es el turno de " + JugActual.getNombre());

			System.out.println("selecciona una opci�n v�lida: ");
			System.out.println("1- ");
			System.out.println("2- ");
			System.out.println("3- ");
			System.out.println("4- ");
			System.out.println("5- ");
			System.out.println("6- ");
			System.out.println("7- ");
			System.out.println("8- ");
			System.out.println("9- ");
			System.out.println("10- ");
			System.out.println("0- Salir");

			int opcion = sc.nextInt();

			switch (opcion) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 0:
				flag = false;
				break;
			default:

			}

			Reassign();
			
		}

	}

	public static void Start() {

		CrearPartida.Iniciar();
		turno = 0;

	}

	public static Propietario TurnBeggining() {

		sc = new Scanner(System.in);

		PropietarioDao prodao = new PropietarioDao();

		return prodao.getByTurn(turno);

	}

	public static void Roll() {

	}

	public static void Reassign() {

		// Sumar turno

	}

	public static boolean Comprar(Propietario j, Propiedad p) {

		PropietarioDao prodao = new PropietarioDao();
		PropiedadDao pd = new PropiedadDao();

		if (p.getPropietario() == null) {
			if (j.getDinero() < p.getPrecio()) {
				return false;
			}
			// le resto el dinero al jugador
			j.setDinero(j.getDinero() - p.getPrecio());
			// a la propiedad le a�ado el jugador
			p.setPropietario(j);
			// al jugador le sumo 1 al contador de propiedades
			j.setCantidadPropiedades(j.getCantidadPropiedades() + 1);
			// guardamos en la base de datos
			prodao.saveOrUpdate(j);
			pd.saveOrUpdate(p);
			return true;

		} else {
			return false;
		}
	}

	public static boolean pagarAlquiler(Propietario j, Propiedad p) {

		PropietarioDao ProDao = new PropietarioDao();

		int casas = p.getNumCasas();
		// el propietario de la casa se llama manolo porque a mana le sale de los huevos
		Propietario manolo = p.getPropietario();

		switch (casas) {
		case 0:
			j.setDinero(j.getDinero() - p.getAlquiler()); // le resto el dinero al jugador
			manolo.setDinero(manolo.getDinero() + p.getAlquiler()); // y a manolo se lo sumo
			break;

		case 1:
			j.setDinero(j.getDinero() - p.getAlquiler1());
			manolo.setDinero(manolo.getDinero() + p.getAlquiler1());
			break;

		case 2:
			j.setDinero(j.getDinero() - p.getAlquiler2());
			manolo.setDinero(manolo.getDinero() + p.getAlquiler2());
			break;

		case 3:
			j.setDinero(j.getDinero() - p.getAlquiler3());
			manolo.setDinero(manolo.getDinero() + p.getAlquiler3());
			break;

		case 4:
			j.setDinero(j.getDinero() - p.getAlquiler4());
			manolo.setDinero(manolo.getDinero() + p.getAlquiler4());
			break;

		case 5:
			j.setDinero(j.getDinero() - p.getAlquilerHotel());
			manolo.setDinero(manolo.getDinero() + p.getAlquilerHotel());
			break;

		default:
			return false;

		}

		ProDao.saveOrUpdate(manolo);
		ProDao.saveOrUpdate(j);
		return true;

	}

	public static int edificar(Propietario j, Propiedad p) {

		PropiedadDao PD = new PropiedadDao();
		PropietarioDao ProDao = new PropietarioDao();

		if (p.getNumCasas() == 5)
			return p.getNumCasas();

		if (j.getDinero() < p.getPrecioCasa())
			return p.getNumCasas();

		if (j.equals(p.getPropietario()))
			return p.getNumCasas();

		j.setDinero(j.getDinero() - p.getPrecioCasa()); // al jugador le resto dinero
		ProDao.saveOrUpdate(j);

		p.setNumCasas(p.getNumCasas() + 1); // a�ado una casa a la propiedad
		PD.saveOrUpdate(p);

		return p.getNumCasas();

	}

	public static boolean hipotecar(Propietario j, Propiedad p) {

		PropietarioDao prodao = new PropietarioDao();
		PropiedadDao pd = new PropiedadDao();

		// si el propietario de la propiedad es igual al jugador que pasamos por
		// parametro
		// + la propiedad no debe de estar hipotecada
		if (p.getPropietario().equals(j) && !p.isHipotecado()) {
			// a�adimos el dinero de la hipoteca al jugador
			j.setDinero(j.getDinero() + p.getPrecioHipoteca());
			// cambiamos el booleano a true
			p.setHipotecado(true);

			prodao.saveOrUpdate(j);
			pd.saveOrUpdate(p);

			return true;

		} else {

			return false;
		}

	}

	public static boolean deshipotecar(Propietario j, Propiedad p) {

		PropietarioDao prodao = new PropietarioDao();
		PropiedadDao pd = new PropiedadDao();

		// si el dinero el jugador tiene el dinero para deshipotecar
		// + la propiedad tiene que pertenecer al jugador
		// + efectivamente est� hipotecado por si eres tonto (si)
		if (j.getDinero() >= p.getPrecioHipoteca() && p.getPropietario().equals(j) && p.isHipotecado()) {
			// le restamos dinero al jugador
			j.setDinero(j.getDinero() - p.getPrecioHipoteca());
			// cambiamos el boolean a false
			p.setHipotecado(false);

			prodao.saveOrUpdate(j);
			pd.saveOrUpdate(p);

			return true;

		} else {
			return false;
		}
	}

}
