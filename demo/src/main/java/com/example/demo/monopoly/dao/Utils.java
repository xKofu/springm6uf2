package com.example.demo.monopoly.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.example.demo.monopoly.Color;
import com.example.demo.monopoly.Ferrocarril;
import com.example.demo.monopoly.Propiedad;
import com.example.demo.monopoly.Propietario;

//import InterceptorsEvents.LoggingInterceptor;

public class Utils {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	static MetadataSources sources;

	public static synchronized SessionFactory getSessionFactory() {
	    if ( sessionFactory == null ) {
	        
//	        serviceRegistry = new StandardServiceRegistryBuilder()
//	                .configure("hibernate.cfg.xml")
//	                .build();
//
//	        sources = new MetadataSources( serviceRegistry );
//	        
//	        Metadata metadata = sources.getMetadataBuilder().build();
//	      
//	        sessionFactory = metadata.getSessionFactoryBuilder().build();
	    	
	    	Configuration conf = new Configuration();
	    	
	    	conf.addAnnotatedClass(Color.class);
	    	conf.addAnnotatedClass(Propiedad.class);
	    	conf.addAnnotatedClass(Propietario.class);
	    	conf.addAnnotatedClass(Ferrocarril.class);
	    	conf.configure("hibernate.cfg.xml");
	    	
	    	ServiceRegistry SR = new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build();
	    	sessionFactory = conf.buildSessionFactory(SR);
	        	
	                
	    }
	    return sessionFactory;
	}

}

