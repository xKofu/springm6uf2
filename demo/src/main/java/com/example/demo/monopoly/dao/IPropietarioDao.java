package com.example.demo.monopoly.dao;

import com.example.demo.monopoly.Propietario;

public interface IPropietarioDao extends IGenericDao<Propietario, Integer>{

	public int modificarDinero(Propietario j, int dineros);
	
	public Propietario Reasignar();

	public Propietario getByTurn(int turno);
	
}
