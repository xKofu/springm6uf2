package com.example.demo.monopoly.dao;

import com.example.demo.monopoly.Color;

public interface IColorDao extends IGenericDao<Color, Integer>{

	
}