package com.example.demo.monopoly.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.example.demo.monopoly.Propiedad;
import com.example.demo.monopoly.Propietario;
import com.example.demo.monopoly.Color;

public class PropiedadDao extends GenericDao<Propiedad, Integer> implements IPropiedadDao{

	public Propietario ComprobarPropietario(Propiedad p) {

		if (p != null)
			return p.getPropietario();

		return null;

	}
	
	
	public boolean ComprobarColor(Propiedad p) {
		
		Color c = p.getColor();

		Set<Propiedad> propiedades = (Set<Propiedad>) c.getPropiedades();
		System.out.println(propiedades);
		
		for (Propiedad propiedad : propiedades) {
			if(propiedad.getPropietario() == null) {
				return false;
				
			}
			
			if (!propiedad.getPropietario().equals(p.getPropietario())) {
			
				return false;
			
			}
		
		}

		return true;
	}
	
}
