package com.example.demo.monopoly;

import com.example.demo.monopoly.dao.ColorDao;
import com.example.demo.monopoly.dao.PropiedadDao;
import com.example.demo.monopoly.dao.PropietarioDao;

public class CrearPartida {

	private static int turno = 0;
	
	public static void main(String[] args) {
		
		Iniciar();
		
	}
	
	public static void Iniciar() {
		
		ColorDao CD = new ColorDao();
		PropiedadDao PD = new PropiedadDao();
		
		Color marron = new Color("Marron");
		CD.saveOrUpdate(marron);
		
		Propiedad tl = new Propiedad("Tienda de Linus");
		tl.setAlquiler(2);
		tl.setAlquiler1(10);
		tl.setAlquiler2(30);
		tl.setAlquiler3(90);
		tl.setAlquiler4(160);
		tl.setAlquilerHotel(250);
		tl.setColor(marron);
		tl.setPrecioCasa(50);
		tl.setPrecioHipoteca(50);
		tl.setPrecio(60);
		tl.setPosicion(1);
		PD.saveOrUpdate(tl);
		
		Propiedad CP = new Propiedad("Caravana de Pam");
		CP.setAlquiler(4);
		CP.setAlquiler1(20);
		CP.setAlquiler2(60);
		CP.setAlquiler3(180);
		CP.setAlquiler4(320);
		CP.setAlquilerHotel(450);
		CP.setColor(marron);
		CP.setPrecioCasa(50);
		CP.setPrecioHipoteca(30);
		CP.setPrecio(60);
		CP.setPosicion(2);
		PD.saveOrUpdate(CP);
		
		Color celeste = new Color("Celeste");
		CD.saveOrUpdate(celeste);
		
		Propiedad CR1 = new Propiedad("Calle del Rio, 1");
		CR1.setAlquiler(6);
		CR1.setAlquiler1(30);
		CR1.setAlquiler2(90);
		CR1.setAlquiler3(270);
		CR1.setAlquiler4(400);
		CR1.setAlquilerHotel(550);
		CR1.setColor(celeste);
		CR1.setPrecioCasa(50);
		CR1.setPrecioHipoteca(50);
		CR1.setPrecio(100);
		CR1.setPosicion(3);
		PD.saveOrUpdate(CR1);
		
		Propiedad CS1 = new Propiedad("Calle del Sauce, 1");
		CS1.setAlquiler(6);
		CS1.setAlquiler1(30);
		CS1.setAlquiler2(90);
		CS1.setAlquiler3(270);
		CS1.setAlquiler4(400);
		CS1.setAlquilerHotel(550);
		CS1.setColor(celeste);
		CS1.setPrecioCasa(50);
		CS1.setPrecioHipoteca(50);
		CS1.setPrecio(100);
		CS1.setPosicion(4);
		PD.saveOrUpdate(CS1);
		
		Propiedad CS2 = new Propiedad("Calle del Sauce, 2");
		CS2.setAlquiler(8);
		CS2.setAlquiler1(40);
		CS2.setAlquiler2(100);
		CS2.setAlquiler3(300);
		CS2.setAlquiler4(450);
		CS2.setAlquilerHotel(600);
		CS2.setColor(celeste);
		CS2.setPrecioCasa(50);
		CS2.setPrecioHipoteca(60);
		CS2.setPrecio(120);
		CS2.setPosicion(5);
		PD.saveOrUpdate(CS2);
		
		Color rosa = new Color("Rosa");
		CD.saveOrUpdate(rosa);
		
		Propiedad TP = new Propiedad("Tienda Pierre's");
		TP.setAlquiler(10);
		TP.setAlquiler1(50);
		TP.setAlquiler2(150);
		TP.setAlquiler3(450);
		TP.setAlquiler4(625);
		TP.setAlquilerHotel(750);
		TP.setColor(rosa);
		TP.setPrecioCasa(100);
		TP.setPrecioHipoteca(70);
		TP.setPrecio(140);
		TP.setPosicion(6);
		PD.saveOrUpdate(TP);
		
		Propiedad CH = new Propiedad("Clinica de Harvey");
		CH.setAlquiler(10);
		CH.setAlquiler1(50);
		CH.setAlquiler2(150);
		CH.setAlquiler3(450);
		CH.setAlquiler4(625);
		CH.setAlquilerHotel(750);
		CH.setColor(rosa);
		CH.setPrecioCasa(100);
		CH.setPrecioHipoteca(70);
		CH.setPrecio(140);
		CH.setPosicion(7);
		PD.saveOrUpdate(CH);
		
		Propiedad HE = new Propiedad("Herreria");
		HE.setAlquiler(12);
		HE.setAlquiler1(60);
		HE.setAlquiler2(180);
		HE.setAlquiler3(500);
		HE.setAlquiler4(700);
		HE.setAlquilerHotel(900);
		HE.setColor(rosa);
		HE.setPrecioCasa(100);
		HE.setPrecioHipoteca(80);
		HE.setPrecio(160);
		HE.setPosicion(8);
		PD.saveOrUpdate(HE);
		
		Color naranja = new Color("Naranja");
		CD.saveOrUpdate(naranja);
		
		Propiedad MU = new Propiedad("Museo");
		MU.setAlquiler(14);
		MU.setAlquiler1(70);
		MU.setAlquiler2(200);
		MU.setAlquiler3(550);
		MU.setAlquiler4(750);
		MU.setAlquilerHotel(950);
		MU.setColor(naranja);
		MU.setPrecioCasa(100);
		MU.setPrecioHipoteca(90);
		MU.setPrecio(180);
		MU.setPosicion(9);
		PD.saveOrUpdate(MU);
		
		Propiedad SFE = new Propiedad("Salon Fruta Estelar");
		SFE.setAlquiler(14);
		SFE.setAlquiler1(70);
		SFE.setAlquiler2(200);
		SFE.setAlquiler3(550);
		SFE.setAlquiler4(750);
		SFE.setAlquilerHotel(950);
		SFE.setColor(naranja);
		SFE.setPrecioCasa(100);
		SFE.setPrecioHipoteca(90);
		SFE.setPrecio(180);
		SFE.setPosicion(10);
		PD.saveOrUpdate(SFE);
		
		Propiedad TDP = new Propiedad("Tienda de Pesca");
		TDP.setAlquiler(16);
		TDP.setAlquiler1(80);
		TDP.setAlquiler2(220);
		TDP.setAlquiler3(600);
		TDP.setAlquiler4(800);
		TDP.setAlquilerHotel(1000);
		TDP.setColor(naranja);
		TDP.setPrecioCasa(100);
		TDP.setPrecioHipoteca(100);
		TDP.setPrecio(200);
		TDP.setPosicion(11);
		PD.saveOrUpdate(TDP);
		
		Color rojo = new Color("Rojo");
		CD.saveOrUpdate(rojo);
		
		Propiedad CL = new Propiedad("Casa de Leah");
		CL.setAlquiler(18);
		CL.setAlquiler1(90);
		CL.setAlquiler2(250);
		CL.setAlquiler3(700);
		CL.setAlquiler4(875);
		CL.setAlquilerHotel(1050);
		CL.setColor(rojo);
		CL.setPrecioCasa(150);
		CL.setPrecioHipoteca(110);
		CL.setPrecio(220);
		CL.setPosicion(12);
		PD.saveOrUpdate(CL);
		
		Propiedad RM = new Propiedad("Rancho de Marnie");
		RM.setAlquiler(18);
		RM.setAlquiler1(90);
		RM.setAlquiler2(250);
		RM.setAlquiler3(700);
		RM.setAlquiler4(875);
		RM.setAlquilerHotel(1050);
		RM.setColor(rojo);
		RM.setPrecioCasa(150);
		RM.setPrecioHipoteca(110);
		RM.setPrecio(220);
		RM.setPosicion(13);
		PD.saveOrUpdate(RM);
		
		Propiedad TM = new Propiedad("Torre del Mago");
		TM.setAlquiler(20);
		TM.setAlquiler1(100);
		TM.setAlquiler2(300);
		TM.setAlquiler3(750);
		TM.setAlquiler4(925);
		TM.setAlquilerHotel(1100);
		TM.setColor(rojo);
		TM.setPrecioCasa(150);
		TM.setPrecioHipoteca(120);
		TM.setPrecio(240);
		TM.setPosicion(14);
		PD.saveOrUpdate(TM);
		
		Color amarillo = new Color("Amarillo");
		CD.saveOrUpdate(amarillo);
		
		Propiedad CE = new Propiedad("Caba�a de Elliot");
		CE.setAlquiler(22);
		CE.setAlquiler1(110);
		CE.setAlquiler2(330);
		CE.setAlquiler3(800);
		CE.setAlquiler4(975);
		CE.setAlquilerHotel(1150);
		CE.setColor(amarillo);
		CE.setPrecioCasa(150);
		CE.setPrecioHipoteca(130);
		CE.setPrecio(260);
		CE.setPosicion(15);
		PD.saveOrUpdate(CE);
		
		Propiedad MI = new Propiedad("Minas");
		MI.setAlquiler(22);
		MI.setAlquiler1(110);
		MI.setAlquiler2(330);
		MI.setAlquiler3(800);
		MI.setAlquiler4(975);
		MI.setAlquilerHotel(1150);
		MI.setColor(amarillo);
		MI.setPrecioCasa(150);
		MI.setPrecioHipoteca(130);
		MI.setPrecio(260);
		MI.setPosicion(16);
		PD.saveOrUpdate(MI);
		
		Propiedad GA = new Propiedad("Gremio de Aventureros");
		GA.setAlquiler(24);
		GA.setAlquiler1(120);
		GA.setAlquiler2(360);
		GA.setAlquiler3(850);
		GA.setAlquiler4(1025);
		GA.setAlquilerHotel(1200);
		GA.setColor(amarillo);
		GA.setPrecioCasa(150);
		GA.setPrecioHipoteca(140);
		GA.setPrecio(280);
		GA.setPosicion(17);
		PD.saveOrUpdate(GA);
		
		Color verde = new Color("verde");
		CD.saveOrUpdate(verde);
		
		Propiedad CA = new Propiedad("Carpinteria");
		CA.setAlquiler(26);
		CA.setAlquiler1(130);
		CA.setAlquiler2(390);
		CA.setAlquiler3(900);
		CA.setAlquiler4(1100);
		CA.setAlquilerHotel(1275);
		CA.setColor(verde);
		CA.setPrecioCasa(200);
		CA.setPrecioHipoteca(150);
		CA.setPrecio(300);
		CA.setPosicion(18);
		PD.saveOrUpdate(CA);
		
		Propiedad MDA = new Propiedad("Mansion del Alcalde");
		MDA.setAlquiler(26);
		MDA.setAlquiler1(130);
		MDA.setAlquiler2(390);
		MDA.setAlquiler3(900);
		MDA.setAlquiler4(1100);
		MDA.setAlquilerHotel(1275);
		MDA.setColor(verde);
		MDA.setPrecioCasa(200);
		MDA.setPrecioHipoteca(150);
		MDA.setPrecio(300);
		MDA.setPosicion(19);
		PD.saveOrUpdate(MDA);
		
		Propiedad CAS = new Propiedad("Casino");
		CAS.setAlquiler(28);
		CAS.setAlquiler1(150);
		CAS.setAlquiler2(450);
		CAS.setAlquiler3(1000);
		CAS.setAlquiler4(1200);
		CAS.setAlquilerHotel(1400);
		CAS.setColor(verde);
		CAS.setPrecioCasa(200);
		CAS.setPrecioHipoteca(160);
		CAS.setPrecio(320);
		CAS.setPosicion(20);
		PD.saveOrUpdate(CAS);
		
		Color azul = new Color("Azul");
		CD.saveOrUpdate(azul);
		
		Propiedad AJ = new Propiedad("Almacen de Joja");
		AJ.setAlquiler(35);
		AJ.setAlquiler1(175);
		AJ.setAlquiler2(500);
		AJ.setAlquiler3(1100);
		AJ.setAlquiler4(1300);
		AJ.setAlquilerHotel(1500);
		AJ.setColor(azul);
		AJ.setPrecioCasa(200);
		AJ.setPrecioHipoteca(175);
		AJ.setPrecio(350);
		AJ.setPosicion(21);
		PD.saveOrUpdate(AJ);
		
		Propiedad CC = new Propiedad("Centro civico");
		CC.setAlquiler(50);
		CC.setAlquiler1(200);
		CC.setAlquiler2(600);
		CC.setAlquiler3(1400);
		CC.setAlquiler4(1700);
		CC.setAlquilerHotel(2000);
		CC.setColor(azul);
		CC.setPrecioCasa(200);
		CC.setPrecioHipoteca(200);
		CC.setPrecio(400);
		CC.setPosicion(22);
		PD.saveOrUpdate(CC);
		
		Color blanco = new Color("Blanco");
		CD.saveOrUpdate(blanco);
		
		
		Ferrocarril bus = new Ferrocarril("Parada de Bus");
		bus.setPrecio(200);
		bus.setColor(blanco);
		bus.setHipoteca(100);
		PD.saveOrUpdate(bus);
		
		Ferrocarril pueblo = new Ferrocarril("Pueblo");
		pueblo.setPrecio(200);
		pueblo.setColor(blanco);
		pueblo.setHipoteca(100);
		PD.saveOrUpdate(pueblo);
		
		Ferrocarril mina = new Ferrocarril("Mina");
		mina.setPrecio(200);
		mina.setColor(blanco);
		mina.setHipoteca(100);
		PD.saveOrUpdate(mina);
		
		Ferrocarril cantera = new Ferrocarril("Cantera");
		cantera.setPrecio(200);
		cantera.setColor(blanco);
		cantera.setHipoteca(100);
		PD.saveOrUpdate(cantera);
		
		bus.getMisVecinos().add(pueblo);
		bus.getMisVecinos().add(cantera);
		
		pueblo.getMisVecinos().add(mina);
		pueblo.getMisVecinos().add(bus);
		
		mina.getMisVecinos().add(cantera);
		mina.getMisVecinos().add(pueblo);
		
		cantera.getMisVecinos().add(mina);
		cantera.getMisVecinos().add(bus);
		
		PD.saveOrUpdate(bus);
		PD.saveOrUpdate(pueblo);
		PD.saveOrUpdate(mina);
		PD.saveOrUpdate(cantera);
		
		//los jugadores empiezan con 1500 de base
		PropietarioDao jd = new PropietarioDao();
		Propietario mana = new Propietario("Mana");
		mana.setTurno(turno++);
		jd.saveOrUpdate(mana);
		
		Propietario gerard = new Propietario("gerard");
		gerard.setTurno(turno++);
		jd.saveOrUpdate(gerard);
		
		Propietario kane = new Propietario("kane");
		kane.setTurno(turno++);
		jd.saveOrUpdate(kane);
		System.out.println("de locos");
		
	}

}
