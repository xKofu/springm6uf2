package com.example.demo.monopoly.dao;

import com.example.demo.monopoly.Propiedad;
import com.example.demo.monopoly.Propietario;

public interface IPropiedadDao extends IGenericDao<Propiedad, Integer>{

	public Propietario ComprobarPropietario(Propiedad p);
	
	public boolean ComprobarColor(Propiedad p);
	
}
