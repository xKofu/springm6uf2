package cositas.propiedad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "ferrocarril")
@JsonIgnoreProperties({"MisVecinos", "SoyVecino"})
public class Ferrocarril extends Propiedad {

	@Column(name="precio")
	private int precio;
	
	@Column(name="hipoteca")
	private int hipoteca;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JsonProperty("MisVecinos")
	@JoinTable(
	name = "Ferrocarril_vecino",
	joinColumns = { @JoinColumn(name = "id_ferrocarril") },
	inverseJoinColumns = { @JoinColumn(name = "id_vecino") }
	)
	private Set<Ferrocarril> MisVecinos;
	
	@ManyToMany(mappedBy = "MisVecinos")
	@JsonProperty("SoyVecino")
	private Set<Ferrocarril> SoyVecino;
		
	public Ferrocarril() {
		super();
		MisVecinos = new HashSet<Ferrocarril>();
		SoyVecino = new HashSet<Ferrocarril>();
	}
	
	
	public Ferrocarril(String nom) {
		super(nom);
		this.MisVecinos = new HashSet<Ferrocarril>();
		this.SoyVecino = new HashSet<Ferrocarril>();
	}


	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getHipoteca() {
		return hipoteca;
	}

	public void setHipoteca(int hipoteca) {
		this.hipoteca = hipoteca;
	}

	public Set<Ferrocarril> getMisVecinos() {
		return MisVecinos;
	}

	public void setMisVecinos(Set<Ferrocarril> misVecinos) {
		MisVecinos = misVecinos;
	}

	public Set<Ferrocarril> getSoyVecino() {
		return SoyVecino;
	}

	public void setSoyVecino(Set<Ferrocarril> soyVecino) {
		SoyVecino = soyVecino;
	}
		
}
