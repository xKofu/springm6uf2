package cositas.propiedad;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete


public interface PropiedadRepository extends CrudRepository<Propiedad, Integer> {
	  //st<Almohada> findByTipoIgnoreCaseOrderByDibujoAsc(String tipo);
	
	Optional<Propiedad> findById(Integer id);
	
	Optional<Propiedad> findByNombreIgnoreCase(String nombre);
	
}
