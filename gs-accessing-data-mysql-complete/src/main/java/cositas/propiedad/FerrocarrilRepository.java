package cositas.propiedad;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface FerrocarrilRepository extends CrudRepository<Ferrocarril, Integer> {
	// st<Almohada> findByTipoIgnoreCaseOrderByDibujoAsc(String tipo);

	Optional<Ferrocarril> findById(Integer id);

}
