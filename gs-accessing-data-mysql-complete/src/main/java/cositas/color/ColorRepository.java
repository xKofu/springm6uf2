package cositas.color;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ColorRepository extends CrudRepository<Color, Integer> {
	// st<Almohada> findByTipoIgnoreCaseOrderByDibujoAsc(String tipo);

	Optional<Color> findById(Integer id);

}
