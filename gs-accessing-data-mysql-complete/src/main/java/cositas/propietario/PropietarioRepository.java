package cositas.propietario;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import cositas.propiedad.Propiedad;

public interface PropietarioRepository extends CrudRepository<Propietario, Integer> {
	// st<Almohada> findByTipoIgnoreCaseOrderByDibujoAsc(String tipo);

	Optional<Propietario> findById(Integer id);

}
