package cositas.propietario;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import cositas.color.Color;
import cositas.propiedad.Propiedad;

@Entity
@Table(name = "propietario")
public class Propietario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_propietario")
	private int id;

	@Column(name="nombre")
	private String nombre;

	@Column(name="vivo")
	private boolean vivo;
	
	@Column(name="turno")
	private int turno;
	
	@Column(name="cantidad_propiedades")
	private int cantidadPropiedades;
	
	@Column(name="contador_victorias")
	private int contadorVictorias;
	
	@Column(name="dinero")
	private int dinero;

	@Column(name="posicion")
	private int posicion;
	
	//mapped = NOMBRE DE LA VARIABLE DEL ENTITY TABLE
	@OneToMany(mappedBy = "propietario")
	@JsonBackReference
	//@Column(name="propiedades", nullable=true)
	private Set<Propiedad> propiedades;

	@OneToMany(mappedBy = "propietario")
	//@Column(name="colores", nullable=true)
	private Set<Color> colores;
	
	public Propietario() {
		super();
		
	}
	
	public Propietario(String nombre) {
		super();
		this.nombre = nombre;
		this.dinero=1500;
		this.cantidadPropiedades=0;
		this.colores=null;
		this.turno=0; //ya miraremos esto
		this.vivo=true;
	}

	public Set<Color> getColores() {
		return colores;
	}


	public void setColores(Set<Color> colores) {
		this.colores = colores;
	}

	public Set<Propiedad> getPropiedades() {
		return propiedades;
	}


	public void setPropiedades(Set<Propiedad> propiedades) {
		this.propiedades = propiedades;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public int getTurno() {
		return turno;
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public int getCantidadPropiedades() {
		return cantidadPropiedades;
	}

	public void setCantidadPropiedades(int cantidadPropiedades) {
		this.cantidadPropiedades = cantidadPropiedades;
	}

	public int getContadorVictorias() {
		return contadorVictorias;
	}

	public void setContadorVictorias(int contadorVictorias) {
		this.contadorVictorias = contadorVictorias;
	}

	public int getDinero() {
		return dinero;
	}

	public void setDinero(int dinero) {
		this.dinero = dinero;
	}

	public int getId() {
		return id;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
	
	@Override
	public String toString() {
		return "Propietario [id=" + id + ", nombre=" + nombre + ", vivo=" + vivo + ", turno=" + turno
				+ ", cantidadPropiedades=" + cantidadPropiedades + ", contadorVictorias=" + contadorVictorias
				+ ", dinero=" + dinero + "]";
	}
	
}
